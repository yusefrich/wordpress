<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{ ?>


<section id="backlog-table">
    <div class=" container-large mx-auto ">

	<?php
	while ($query->have_posts())
	{
		$query->the_post();
    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>


        <table class="table table-responsive-sm px-md-4 mb-100">
            <thead>
                <tr>
                	<?php if ( is_user_logged_in() ) : ?>
                    	<th scope="col">Prioridade</th>
                	<?php endif; ?>
                    <th scope="col">Tipo</th>
                    <th scope="col">Esforço</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Status</th>
                    <th scope="col">Início</th>
                    <th scope="col">Fim Previsto</th>
                	<?php if ( is_user_logged_in() ) : ?>
                    	<th scope="col">Observação</th>
                	<?php endif; ?>
                </tr>
            </thead>

            <tbody>



                <?php if ( have_rows('tarefa') ): ?>
                        <?php while ( have_rows('tarefa') ) : the_row(); ?>
                    <tr>
                		<?php if ( is_user_logged_in() ) : ?>
		                    <td class="align-middle">
		                        <?php if( get_sub_field('prioridade')): ?>
			                    	<?php the_sub_field('prioridade'); ?>
			                    <?php endif; ?>
		                    </td>
                		<?php endif; ?>
		                    <td class="align-middle">

		                        <?php if( get_sub_field('tipo')): 
		                        	$tipo = get_sub_field('tipo');

		                        	?>
			                    	<?php echo esc_html( $tipo->name ); ?>
			                    <?php endif; ?>
		                    </td>
		                    <td class="align-middle">

		                        <?php if( get_sub_field('esforco')): 
		                        	$esforco = get_sub_field('esforco'); ?>
			                    	<?php echo esc_html( $esforco->name ); ?>
			                    <?php endif; ?>
		                    </td>
		                    <td class="align-middle" style=" max-width: 162px;">

		                        <?php if( get_sub_field('descricao')): ?>
			                    	<?php the_sub_field('descricao'); ?>
			                    <?php endif; ?>
			                </td>
		                    <td class="align-middle">
		                        <?php if( get_sub_field('status')): 
		                        	$status = get_sub_field('status'); ?>
			                    	<?php echo esc_html( $status->name ); ?>
			                    <?php endif; ?>
			                    	
			                </td>
		                    <td class="align-middle">
		                        <?php if( get_sub_field('inicio')): ?>
			                    	<?php the_sub_field('inicio'); ?>
			                    <?php endif; ?>
			                    
			                </td>
		                    <td class="align-middle">

		                        <?php if( get_sub_field('fim')): ?>
			                    	<?php the_sub_field('fim'); ?>
			                    <?php endif; ?>
		                    </td>
                		<?php if ( is_user_logged_in() ) : ?>
		                    <td class="align-middle" style=" max-width: 162px;">
		                    	
		                        <?php if( get_sub_field('observacoes')): ?>
			                    	<?php the_sub_field('observacoes'); ?>
			                    <?php endif; ?>

		                    </td>
                		<?php endif; ?>
	                    </tr>
                        <?php endwhile; ?>
                <?php endif; ?>


            </tbody>
        </table>
		<?php
	} ?>


                
    </div>
</section>

	<?php if ( have_rows('sprint') ): ?>
<section id="sprints-title" class="bg-blue">
    <div class="container">
        <div data-aos="fade-up" class="row my-2 py-5 ">
            <div class="col-md-4">
                <h2 class="text-orange font-weight-bold mt-3 float-md-right mr-3">Sprints</h2>
            </div>
            <div class="col-md-8">
                <p style="max-width: 712px;" class="text-caption">

                    <?php the_field('sprints', '395'); ?>
                    
                </p>
            </div>
        </div>

    </div>
</section>
<section id="spints" class="bg-blue pb-72">
    <div class="container-large mx-auto container-height-sm">
        <div class="swiper-container">
            <div class="swiper-wrapper">
            	<?php while ( have_rows('sprint') ) : the_row(); ?>
				<div class="sprint-class"  data-sprint-init="<?php the_sub_field('data_inicio'); ?>"></div>
                <div class="swiper-slide "> 
                    <a href="#sprints-review-title" target="sprint<?php the_sub_field('num_sprint'); ?>" class="link-review" >
                        <div class="sprint-card">
		                        
                            <h3 class="text-dark">Sprint <?php the_sub_field('num_sprint'); ?></h3>
                            <p class="text-gray-5">
								<span class="text-gray-5">
									<?php if( get_sub_field('data_inicio')): ?>
										<?php the_sub_field('data_inicio'); ?>
									<?php endif; ?>
								</span>
			                    -
								<span class="text-gray-5">
									<?php if( get_sub_field('data_fim')): ?>
										<?php the_sub_field('data_fim'); ?>
									<?php endif; ?>
								</span>
			                </p>



                            <ul>
			                	<?php $num = get_sub_field('num_sprint'); 
			                	if ( have_rows('tarefa') ): ?>
			                        <?php while ( have_rows('tarefa') ) : the_row(); ?>
			                        	<?php if( get_sub_field('numero_sprint') == $num): 
			                        		if( get_sub_field('nome')): ?>
                                			<li class="text-gray-6"><?php the_sub_field('nome'); ?></li>
                                		<?php endif; 
                                	endif; ?>
                                	<?php endwhile; ?>
			                	<?php endif; ?>
                            </ul>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
    
            </div>
        </div>
    </div>
</section>




<?php if ( is_user_logged_in() ) : ?>
<section id="sprints-review-title" class="bg-blue d-none">
    <div class="container">
        <div data-aos="fade-up" class="row my-2 py-5 ">
            <div class="col-md-6">
                <h2 class="text-orange font-weight-bold mt-3 float-md-right mr-3">Sprint Review</h2>
            </div>
            <div class="col-md-6">
                <p style="max-width: 481px;" class="text-caption">
                    <?php the_field('sprints_review', '395'); ?>
                </p>
            </div>
        </div>

    </div>
</section>
<section id="spints-review" class="bg-blue d-none">
    <div class="container-large mx-auto">
        <div class="row">
            <div class="col-md-12"> 


            	<?php while ( have_rows('sprint') ) : the_row(); ?>

	                <div class="sprint-card" id="sprint<?php the_sub_field('num_sprint'); ?>">
	                    <h3 id="titleEl" class="text-orange-energisa">Sprint <?php the_sub_field('num_sprint'); ?></h3>
	                    <p id="dateEl" class="text-gray-5">
		                        <?php if( get_sub_field('data_inicio')): ?>
			                    	<?php the_sub_field('data_inicio'); ?>
			                    <?php endif; ?>
			                    -
		                        <?php if( get_sub_field('data_fim')): ?>
			                    	<?php the_sub_field('data_fim'); ?>
			                    <?php endif; ?>

	                    </p>


							<?php 
							$usuarios = get_sub_field('usuarios');
							$num = get_sub_field('num_sprint'); 
							if( $usuarios ): ?>

							    <?php foreach( $usuarios as $us ): ?>

	                    		<div class="each-user mb-2 mt-2">

				                    <div class="d-flex justify-content-start">
				                        <img id="userPictureEl" class="user-pic" src="<?php echo get_avatar_url($user->ID, ['size' => '100']); ?>" alt="">
				                        <p><strong id="userEl" class="text-dark"><?php echo $us->display_name; ?></strong></p>
				                    </div>



					                	<?php if ( have_rows('tarefa') ): ?>
					                		<ul>
					                        <?php while ( have_rows('tarefa') ) : the_row(); ?>
					                        	<?php if( get_sub_field('numero_sprint') == $num): ?>

							                        <?php if( get_sub_field('usuario')): 
							                        	$user = get_sub_field('usuario'); ?>
					                        			<?php if( $user->ID == $us->ID): ?>
									                        <li class="text-gray-6 d-flex justify-content-between">
									                            <span class="text-gray-6">


										                        <?php if( get_sub_field('status')): 
										                        	$status = get_sub_field('status'); ?>
										                        	<img class="emoji-icon" src="<?php echo get_field('foto', $status); ?>" alt="">
											                    <?php endif; ?>
									                             [<?php the_sub_field('porcentagem'); ?>%] – <?php the_sub_field('nome'); ?></span>
									                            <span class="text-dark">

											                        <?php if( get_sub_field('fim')): ?>
												                    	Prazo: <?php the_sub_field('fim'); ?>
												                    <?php endif; ?>
									                            </span>
									                        </li>
					                        			<?php endif; ?>
								                    <?php endif; ?>
		                                		<?php endif; ?>
		                                	<?php endwhile; ?>
		                                	</ul>
					                	<?php endif; ?>


	                    		</div>
									<hr>
							    <?php endforeach; ?>

							<?php endif; ?>
			                    

			                    <?php // echo esc_html( $usuario->term_id ); ?>

	                </div>

	            <?php endwhile; ?>



            </div>
        </div>
    </div>

</section>
<?php endif; ?>
<?php endif; ?>
	<?php
}
else
{
	echo "<h3 class='none'>Não encontramos nenhum evento nesta categoria.</h3>";
}
?>