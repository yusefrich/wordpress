<?php get_header(); /* Template Name: Backlog */ ?>
<section id="backlog-banner">
    <div style="
            background: linear-gradient(0deg, #5F817E, #5F817E), url(<?php bloginfo('template_url'); ?>/img/backlog-header-bnw.png);
            background-size: cover;
            background-position: center;
            background-blend-mode: multiply, normal;
            " class="container-fluid   product-banner-holder px-0 ">
        <!-- text-white -->
        <div class="backlog-banner text-center text-white ">
            <h2 data-aos="fade-right">
                    <?php the_field('titulo'); ?>
            </h2>
            <p sty>
                    <?php the_field('subtitulo'); ?>

               </p>
        </div>
    </div>
</section>
<div class="container">
    <div id="parallax-detalhe-2">
        <div data-depth="0.2" class=" d-flex justify-content-between">
            <div class="d-none d-md-block trace-detail-left trace-detail-offset-up">
                <img class="" src="<?php bloginfo('template_url'); ?>/img/detalhes-traco-light.png" alt="">
            </div>
            <div style="z-index: 1;" class="trace-detail-right ">
            </div>
        </div>
    </div>
</div>
<section id="proposito">
    <div class="container">
        <div data-aos="fade-up" class="row my-2 py-5 ">
            <div class="col-md-4">
                <h2 class="text-orange font-weight-bold mt-3 float-md-right mr-3">Backlog</h2>
            </div>
            <div class="col-md-8">
                <p style="max-width: 712px;" class="text-caption text-dark">
                    <?php the_field('backlog'); ?>
                </p>
            </div>
        </div>

        <hr>
    </div>
</section>
<section id="backlog-list">
    <div class="d-flex justify-content-center search-spacing">
        <p class="font-weight-bold mr-2 mt-1 text-gray-4">Escolha um produto</p>
        
        <!--


        <select style="width: 25%; color: #EA6724;" class="custom-select" id="loadPerCategory">
            <option value="">Selecione</option>
            <?php
        $categoria_lista = get_categories(
            array(
                'parent' => 0
            )
        );
        foreach ($categoria_lista as $category): ?>
            <option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
            <?php endforeach; ?>
        </select> 



        onclick="setSprintReview('Sprint <?php the_sub_field('num_sprint'); ?>', '<?php if( get_sub_field('data_inicio')): the_sub_field('data_inicio'); endif; ?> - <?php if( get_sub_field('data_fim')):the_sub_field('data_fim'); endif; ?>', 'Eduardo Lemos', '<?php bloginfo('template_url'); ?>/img/sprint-profile.png', 'default')"
        -->

        <?php echo do_shortcode( '[searchandfilter id="426"]' ); ?>

    </div>
</section>
<section id="backlog-table-empity" class="d-none">
    <div class="centerMessageHolder">
            <p>Selecione um item acima para mostrar os itens...</p>
    </div>

</section>



        <?php echo do_shortcode( '[searchandfilter id="426" show="results"]' ); ?>


<style type="text/css">
    .searchandfilter{
        width: 25% !important;
    }
    .searchandfilter label,
    .searchandfilter *,
    .searchandfilter ul{
        width: 100% !important; 

    }
    .searchandfilter ul li{
        padding: 0;
        width: 100% !important; 
    }
    .searchandfilter ul li select{
        color: #EA6724;
        min-width: 100% !important;
        width: 100% !important; 
    }
    #spints-review .sprint-card{
        display: none;
    }
    #spints-review .sprint-card.active{
        display: block;
    }
    #spints-review .sprint-card hr:last-child{
        display: none;
    }
</style>
<script type="text/javascript">
    jQuery('.searchandfilter ul li select').addClass('custom-select');


    jQuery('.searchandfilter ul li select').change(function () {
        jQuery('.searchandfilter ul li select').addClass('custom-select');
    });
    jQuery( document ).ajaxComplete(function( event,request, settings ) {
        jQuery('.searchandfilter ul li select').addClass('custom-select');
    });

jQuery(document).delegate('a.link-review', 'click', function(event) {
    event.preventDefault();


    jQuery('section#sprints-review-title').removeClass('d-none');
    jQuery('section#spints-review').removeClass('d-none');

    var id = jQuery(this).attr('target');
    jQuery('.sprint-card').removeClass('active');
    jQuery('.sprint-card#' + id).addClass('active');





    var link = jQuery(this).attr('href');
    var values = link.split('#');
    var pag = values[0];
    var tab = '#' + values[1];
    var divPosition = jQuery(tab).offset();
    var scrollPosition = divPosition.top - 50;
    jQuery('html, body').animate({scrollTop: scrollPosition}, 200);
});
</script>
<?php include "footer-nav.php"; ?>

<?php get_footer(); ?>